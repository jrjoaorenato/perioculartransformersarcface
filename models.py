import torch
from torch import nn
from torch.nn import functional as F
from torch.optim.lr_scheduler import LinearLR, ConstantLR, SequentialLR, CosineAnnealingLR, ReduceLROnPlateau, CosineAnnealingWarmRestarts, ExponentialLR
from pytorch_lightning.callbacks import LearningRateMonitor
import torchvision.models as models
from pytorch_metric_learning import losses
from pytorch_metric_learning import regularizers
import numpy as np
from transformers import AutoFeatureExtractor, ViTForImageClassification
from deit_models import deit_small_patch16_224, deit_tiny_patch16_224, deit_base_patch16_224
import os

import pytorch_lightning as pl

from resnest.torch import resnest50

import timm
from timm.optim import Lamb

class ModelLoss(torch.nn.Module):
    def __init__(self, loss_func=None):
        super().__init__()
        self.loss_func = loss_func

    def forward(self, pred, target):
        loss = self.loss_func(pred, target)
        return loss

class PeriocularArcFace(pl.LightningModule):
    def __init__(self, num_classes, learning_rate = 1e-4, embedding_size = 1024, margin = 17.6, scale = 16):
        super().__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.embedding_size = embedding_size
        self.margin = margin
        self.scale = scale
        # self.backbone = models.resnet50(pretrained=True)
        self.backbone = resnest50(pretrained=True)
        self.backbone.fc = nn.Linear(self.backbone.fc.in_features, self.embedding_size)
        R = regularizers.RegularFaceRegularizer()
        loss_func = losses.ArcFaceLoss(self.num_classes, self.embedding_size, margin=self.margin, scale=self.scale, weight_regularizer=R)
        self.model_loss = ModelLoss(loss_func=loss_func)
        self.feat = np.empty([0,self.embedding_size])
        self.label = np.empty(0)

    def forward(self, x):
        embedding = self.backbone(x)
        return embedding

    def configure_optimizers(self):
        params = list(self.backbone.parameters()) + list(self.model_loss.parameters())
        # optimizer = torch.optim.Adam(params, lr=(self.learning_rate))
        # scheduler = ReduceLROnPlateau(optimizer, 'min')
        optimizer = torch.optim.AdamW(params, lr=(self.learning_rate), weight_decay=0.05)
        #scheduler = CosineAnnealingWarmRestarts(optimizer, T_0=10, T_mult=2)
        scheduler = LinearLR(optimizer, start_factor=1.0, total_iters=1)
        # scheduler2 = CosineAnnealingLR(optimizer, T_max=280)
        return {
           'optimizer': optimizer,
           'lr_scheduler': scheduler, # Changed scheduler to lr_scheduler
       }

    def training_step(self, train_batch, batch_idx):
        x, y = train_batch
        z = self.backbone(x)
        loss = self.model_loss(z, y)
        self.log('train_loss', loss)
        # self.log('lr', self.learning_rate)
        return loss

    def validation_step(self, val_batch, batch_idx):
        x, y = val_batch
        z = self.backbone(x)
        loss = self.model_loss(z, y)
        self.log('val_loss', loss)

    def test_step(self, test_batch, batch_idx):
        x, y = test_batch
        z = self.backbone(x)
        self.feat = np.vstack((self.feat, z.cpu().detach().numpy()))
        self.label = np.append(self.label, y.cpu().detach().numpy())

    def save_output(self, location = '.', dataset = 'ubipr'):
        filename_emb = str(location) + '/' + str(dataset) + '_embedding.npy'
        filename_label = str(location) + '/' + str(dataset) + '_labels.npy'
        with open(filename_emb, 'wb') as f:
            np.save(f, self.feat)
        with open(filename_label, 'wb') as f:
            np.save(f, self.label)
            
class PeriocularViT(pl.LightningModule):
    def __init__(self, num_classes, learning_rate = 1e-3, embedding_size = 1024, margin = 17.6, scale = 16):
        super().__init__()
        self.num_classes = num_classes
        self.learning_rate = learning_rate
        self.embedding_size = embedding_size
        self.margin = margin
        self.scale = scale

        #self.backbone = timm.create_model('vit_small_patch32_224', pretrained=True, num_classes = self.embedding_size) 
        self.backbone = timm.create_model('vit_tiny_patch16_224', pretrained=True, num_classes = self.embedding_size) 
        # self.backbone = timm.create_model('deit_tiny_patch16_224', pretrained=True, num_classes = self.embedding_size) 
        # self.backbone = timm.create_model('efficientformer_l1', pretrained=True, num_classes = self.embedding_size) 
        # ViTForImageClassification.from_pretrained('facebook/deit-tiny-patch16-224') #Hugginface 
        # self.backbone = deit_base_patch16_224(pretrained=True) #Facebook
        #self.backbone = deit_tiny_patch16_224(pretrained=True) #Facebook
        #self.backbone.head = nn.Linear(in_features = self.backbone.norm.weight.shape[0], out_features = self.embedding_size, bias = True)
        self.backbone.train()
        # self.backbone = ViTForImageClassification.from_pretrained('facebook/deit-tiny-patch16-224')
        # self.backbone.classifier = nn.Linear(in_features = 192, out_features = self.embedding_size, bias = True)
   

        # self.backbone_fc = nn.Linear(2048, self.embedding_size)
        R = regularizers.RegularFaceRegularizer()
        loss_func = losses.ArcFaceLoss(self.num_classes, self.embedding_size, margin=self.margin, scale=self.scale, weight_regularizer=R)
        self.model_loss = ModelLoss(loss_func=loss_func)
        self.feat = np.empty([0,self.embedding_size])
        self.label = np.empty(0)

    def forward(self, x):
        embedding = self.backbone(x)
        # embedding = self.backbone_fc(embedding)
        return embedding

    def configure_optimizers(self):
        params = list(self.backbone.parameters()) + list(self.model_loss.parameters())
        # optimizer = torch.optim.Adam(params, lr=(self.learning_rate))
        # optimizer = torch.optim.SGD(params, lr=(self.learning_rate), momentum=0.9, weight_decay=1e-4)
        # scheduler = ReduceLROnPlateau(optimizer, 'min', factor=0.1, patience=5)
        optimizer = Lamb(params, lr=(self.learning_rate), weight_decay=0.02)
        #optimizer = torch.optim.AdamW(params, lr=(self.learning_rate), weight_decay=0.02)
        scheduler1 = ConstantLR(optimizer, factor=1.0, total_iters=5)
        scheduler2 = CosineAnnealingLR(optimizer, T_max=280)
        scheduler = SequentialLR(optimizer, schedulers=[scheduler1, scheduler2], milestones=[20])
        return {
           'optimizer': optimizer,
           'lr_scheduler': scheduler, # Changed scheduler to lr_scheduler
        #    'monitor': 'val_loss'
       }

    def training_step(self, train_batch, batch_idx):
        x, y = train_batch
        z = self.backbone(x)
        # z = self.backbone_fc(z)
        loss = self.model_loss(z, y)
        self.log('train_loss', loss)
        # self.log('lr', self.learning_rate)
        return loss

    def validation_step(self, val_batch, batch_idx):
        x, y = val_batch
        z = self.backbone(x)
        # z = self.backbone_fc(z)
        loss = self.model_loss(z, y)
        self.log('val_loss', loss)

    def test_step(self, test_batch, batch_idx):
        x, y = test_batch
        z = self.backbone(x)
        # z = self.backbone_fc(z)
        self.feat = np.vstack((self.feat, z.cpu().detach().numpy()))
        self.label = np.append(self.label, y.cpu().detach().numpy())

    def save_output(self, location = '.', dataset = 'ubipr'):
        filename_emb = str(location) + '/' + str(dataset) + '_embedding.npy'
        filename_label = str(location) + '/' + str(dataset) + '_labels.npy'
        with open(filename_emb, 'wb') as f:
            np.save(f, self.feat)
        with open(filename_label, 'wb') as f:
            np.save(f, self.label)
