import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torch.utils.data import random_split
from torch.optim.lr_scheduler import ReduceLROnPlateau
import torchvision.models as models
from torchvision import transforms
from dataset import FRGCPeriocularDataModule, FRGCTestDataModule
from dataset import UbiprDataModule
from models import PeriocularArcFace, PeriocularViT
from pytorch_metric_learning import losses
from pytorch_metric_learning import miners
from pytorch_metric_learning import regularizers
import os

import pytorch_lightning as pl

train_dataset = 'ubipr'
test_dataset = 'frgc'

batch_size = 1024
num_classes = 522 #ubipr
# num_classes = 886 #frgc
embedding_size = 1024
frgc_location = './data/FRGC_TEST/'

frgc_dataset = FRGCTestDataModule(frgc_location, batch_size=batch_size)

model = PeriocularViT.load_from_checkpoint("./checkpoints/PViT_l3_UBIPR/last.ckpt", num_classes=num_classes)
model.freeze()

trainer = pl.Trainer(gpus=1)
trainer.test(model, dataloaders=frgc_dataset)

model.save_output('./transf_data/deit/', str(train_dataset + '2' + test_dataset))