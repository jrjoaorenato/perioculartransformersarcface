#geteerinf -p '../../transf_data/deit' -i 'impostor_frgc2ubipr.txt' -g 'genuine_frgc2ubipr.txt' -e 'FRGC to UBIPR_sample' -rf '.tex'
#geteerinf -p "../../transf_data/deit" -i "impostor_frgc2frgc.txt" -g "genuine_frgc2frgc.txt" -e "FRGC to FRGC" -rf '.tex'


import numpy as np
from scipy.spatial.distance import pdist,squareform
from tqdm import tqdm

train_dataset = 'ubipr'
test_dataset = 'frgc'

a = np.load(str('./transf_data/deit/' + train_dataset + '2' + test_dataset + '_embedding.npy'))
print(train_dataset + '2' + test_dataset + " embedding shape: ", a.shape)
y = np.load(str('./transf_data/deit/' + train_dataset + '2' + test_dataset+ '_labels.npy'))
print(train_dataset + '2' + test_dataset + " label shape: ", y.shape)

x = np.triu(1-squareform(pdist(a, metric='cosine')))

print(train_dataset + '2' + test_dataset + " similarity matrix shape: ", x.shape)

np.save(str('./transf_data/deit/similarity_matrix_' + train_dataset + '2' + test_dataset + '.npy'), x)

print(train_dataset + '2' + test_dataset + " similarity matrix saved")

M = x.shape[0]

GenDot = []
ImpDot = []

print(train_dataset + '2' + test_dataset + " generating score files")

for i in tqdm(range(M)):
  for j in range(i, M):
    if (y[i] == y[j] and i != j):
      with open(str('./transf_data/deit/genuine_' + train_dataset + '2' + test_dataset + '.txt'), 'a') as f:
        f.write(str(x[i][j]))
        f.write('\n')

    elif (y[i] != y[j] and i != j):
      with open('./transf_data/deit/impostor_' + train_dataset + '2' + test_dataset + '.txt', 'a') as f:
        f.write(str(x[i][j]))
        f.write('\n')