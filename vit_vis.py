import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torch.utils.data import random_split
from torch.optim.lr_scheduler import ReduceLROnPlateau
import torchvision.models as models
from torchvision import transforms
from dataset import FRGCPeriocularDataModule
from dataset import UbiprDataModule
from dataset import EthnicOcularDataModule
from models import PeriocularArcFace, PeriocularViT
from pytorch_metric_learning import losses
from pytorch_metric_learning import miners
from pytorch_metric_learning import regularizers
import cv2
import numpy as np
from skimage import exposure 
import torchvision.transforms.functional as TF


import vit_rollout

import os

import pytorch_lightning as pl

class UnNormalize(object):
    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, tensor):
        """
        Args:
            tensor (Tensor): Tensor image of size (C, H, W) to be normalized.
        Returns:
            Tensor: Normalized image.
        """
        for t, m, s in zip(tensor, self.mean, self.std):
            t.mul_(s).add_(m)
            # The normalize code -> t.sub_(m).div_(s)
        return tensor

train_dataset = 'frgc'
test_dataset = 'frgc'

batch_size = 128
#num_classes = 522 #ubipr
num_classes = 886 #frgc
embedding_size = 1024

sample = 12
frgc_location = './data/FRGC/whole/'
ubipr_location = './data/UBIPR'
ethnic_train = './data/Ethnic-Ocular/train_data/'
ethnic_test = './data/Ethnic-Ocular/test_data/'

# ethnic_ocular = EthnicOcularDataModule(ethnic_train, ethnic_test, batch_size=batch_size)
ubipr_dataset = UbiprDataModule(ubipr_location, batch_size=batch_size, complete = False, train=False)
# frgc_dataset = FRGCPeriocularDataModule(frgc_location, batch_size=batch_size)

# frgc_dataset.setup()
# frgc_test = frgc_dataset.test_dataloader()
# testset = frgc_test.dataset

ubipr_dataset.setup()
ubipr_test = ubipr_dataset.test_dataloader()
testset = ubipr_test.dataset

# ethnic_ocular.setup()
# ethnic_test = ethnic_ocular.test_dataloader()
# testset = ethnic_test.dataset

test_sample = testset[sample]
img = test_sample[0]
x = img.unsqueeze(0)
y = test_sample[1]

angle = 90
x2 = TF.rotate(x, angle)
img2 = x2.squeeze(0)

model = PeriocularViT.load_from_checkpoint("/content/PeriocularRecognition/checkpoints/ViT/last.ckpt", num_classes=num_classes)
#model = PeriocularViT.load_from_checkpoint("./EfficientFormer/model.ckpt", num_classes=num_classes)
model.freeze()

discard_ratio_ar = [0, 0, 0.9] #0<=x<=1
head_fusion_ar = ['mean', 'min', 'max'] #mean, max, min
name_input = "outputs/input_{}.png".format(y)
name_input_rot = "outputs/input_rot_{}.png".format(y)


unorm = UnNormalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
img = unorm(img)
np_img = np.array(img).transpose(1, 2, 0)[:,:,::-1]
np_img = exposure.rescale_intensity(np_img, out_range=(0, 255))
np_img = np.uint8(np_img)
cv2.imwrite(name_input, np_img)

img2 = unorm(img2)
np_img2 = np.array(img2).transpose(1, 2, 0)[:,:,::-1]
np_img2 = exposure.rescale_intensity(np_img2, out_range=(0, 255))
np_img2 = np.uint8(np_img2)
cv2.imwrite(name_input_rot, np_img2)


for i in range(3):
    discard_ratio = discard_ratio_ar[i]
    head_fusion = head_fusion_ar[i]

    rollout = vit_rollout.VITAttentionRollout(model, discard_ratio=discard_ratio, head_fusion=head_fusion)

    mask = rollout(x)
    mask2 = rollout(x2)

    name = "outputs/grad_rollout_{}_{:.3f}_{}.png".format(y, discard_ratio, head_fusion)
    name2 = "outputs/grad_rollout_rot_{}_{:.3f}_{}.png".format(y, discard_ratio, head_fusion)

    # unorm = UnNormalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
    # img = unorm(img)

    # np_img = np.array(img).transpose(1, 2, 0)[:,:,::-1]
    mask = cv2.resize(mask, (np_img.shape[1], np_img.shape[0]))
    map_img = exposure.rescale_intensity(mask, out_range=(0, 255))
    # np_img = exposure.rescale_intensity(np_img, out_range=(0, 255))
    # np_img = np.uint8(np_img)
    map_img = np.uint8(map_img)

    heatmap_img = cv2.applyColorMap(map_img, cv2.COLORMAP_JET)

    mask = cv2.addWeighted(heatmap_img, 0.5, np_img, 1.0, 0)

    # cv2.imshow("Input Image", np_img)
    # cv2.imshow(name, mask)
    cv2.imwrite(name, mask)
    #####################################################################
    mask2 = cv2.resize(mask2, (np_img.shape[1], np_img2.shape[0]))
    map_img2 = exposure.rescale_intensity(mask2, out_range=(0, 255))
    # np_img = exposure.rescale_intensity(np_img, out_range=(0, 255))
    # np_img = np.uint8(np_img)
    map_img2 = np.uint8(map_img2)

    heatmap_img2 = cv2.applyColorMap(map_img2, cv2.COLORMAP_JET)

    mask2 = cv2.addWeighted(heatmap_img2, 0.5, np_img2, 1.0, 0)

    # cv2.imshow("Input Image", np_img)
    # cv2.imshow(name, mask)
    cv2.imwrite(name2, mask2)


