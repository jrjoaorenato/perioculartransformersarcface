import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torch.utils.data import random_split
from torch.optim.lr_scheduler import ReduceLROnPlateau
import torchvision.models as models
from torchvision import transforms
from dataset import FRGCPeriocularDataModule
from dataset import UbiprDataModule
from models import PeriocularArcFace, PeriocularViT
from pytorch_metric_learning import losses
from pytorch_metric_learning import miners
from pytorch_metric_learning import regularizers
import os

import pytorch_lightning as pl

train_dataset = 'frgc'
test_dataset = 'frgc'

batch_size = 128
# num_classes = 522 #ubipr
num_classes = 886 #frgc
embedding_size = 1024
frgc_location = './data/FRGC/whole/'

frgc_dataset = FRGCPeriocularDataModule(frgc_location, batch_size=batch_size)

frgc_dataset.setup()
frgc_test = frgc_dataset.test_dataloader()
testset = frgc_test.dataset
test_sample = testset[0]
x = test_sample[0]
y = test_sample[1]

model = PeriocularViT.load_from_checkpoint("./EfficientFormer/model.ckpt", num_classes=num_classes)
model.freeze()

attn_data = model.backbone.stages[-1].blocks[-1].token_mixer

B, N, C = x.shape
qkv = attn_data.qkv(x)
qkv = qkv.reshape(B, N, attn_data.num_heads, -1).permute(0, 2, 1, 3)
q, k, v = qkv.split([attn_data.key_dim, attn_data.key_dim, attn_data.val_dim], dim=3)

attent = (q @ k.transpose(-2, -1)) * attn_data.scale
attent = attent + attn_data.get_attention_biases(x.device)

attent = attent.softmax(dim=-1)

x = (attent @ v).transpose(1, 2).reshape(B, N, attn_data.val_attn_dim)
x = attn_data.proj(x)


















# B, N, C = x.shape
# qkv = attn.qkv(x)
# qkv = qkv.reshape(B, N, attn.num_heads, -1).permute(0, 2, 1, 3)
# q, k, v = qkv.split([attn.key_dim, attn.key_dim, attn.val_dim], dim=3)

# attent = (q @ k.transpose(-2, -1)) * attn.scale
# attent = attent + attn.get_attention_biases(x.device)

# attent = attent.softmax(dim=-1)

# x = (attent @ v).transpose(1, 2).reshape(B, N, attn.val_attn_dim)
# x = attn.proj(x)