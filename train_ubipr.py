# import comet_ml
# combine deit with arcface distillation or 
# adapt vit_rollout for EfficientFormer

import torch
import numpy as np
import math
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torch.utils.data import random_split
from torch.optim.lr_scheduler import ReduceLROnPlateau
from pytorch_lightning.callbacks import LearningRateMonitor, ModelCheckpoint
import torchvision.models as models
from torchvision import transforms
from dataset import UbiprDataModule
from models import PeriocularArcFace, PeriocularViT
from pytorch_metric_learning import losses
from pytorch_metric_learning import miners
from pytorch_metric_learning import regularizers
import os

import pytorch_lightning as pl
# from pytorch_lightning.loggers import CometLogger
from pytorch_lightning.loggers import WandbLogger

from torchsummary import summary

# experiment = comet_ml.Experiment(
#     api_key="kknU6sZKXu1ZUCyI1nLpQtKNv",
#     project_name="arcfaceperiocular",
# )

batch_size = 256
num_classes = 522
embedding_size = 1024
scale_par = math.sqrt(2)*math.log(num_classes-1)  #16
ubipr_location = './data/UBIPR'

pl.seed_everything(42, workers=True)

ubipr_dataset = UbiprDataModule(ubipr_location, batch_size=batch_size, complete=False, train=True)

# model
# model
# model = PeriocularArcFace(num_classes=num_classes, learning_rate=0.0001, embedding_size=embedding_size, margin=17.6, scale=scale_par)


model = PeriocularViT(num_classes=num_classes, embedding_size=embedding_size, margin=17.6, scale=scale_par, learning_rate=1e-3)
# model = PeriocularViT.load_from_checkpoint("./deit/epoch=14-step=3195.ckpt", num_classes=num_classes)




# print(model)



# comet_logger = CometLogger(
    # api_key="kknU6sZKXu1ZUCyI1nLpQtKNv",
    # rest_api_key="kknU6sZKXu1ZUCyI1nLpQtKNv",
    # project_name="arcfaceperiocular",
#    optimizer_data=model.parameters,
# )

wandb_logger = WandbLogger(project="periocularArcface")

# # training
# trainer = pl.Trainer(gpus=1, logger=comet_logger, max_epochs=100, deterministic=True)
lr_monitor = LearningRateMonitor(logging_interval='step')
checkpoint_callback = ModelCheckpoint(dirpath='checkpoints/PViT_l3_UBIPR', save_top_k = 1, monitor='val_loss', save_last=True)
trainer = pl.Trainer(logger=wandb_logger,
                    gradient_clip_val=0.5,
                    max_epochs=200,
                    deterministic=False,
                    auto_lr_find=False,
                    accelerator="gpu",
                    devices = 1,
                    # accelerator="tpu",
                    # devices=8,
                    callbacks=[lr_monitor, checkpoint_callback])

trainer.fit(model, ubipr_dataset)#, ckpt_path="deit/last-v2.ckpt")
trainer.save_checkpoint("./models/PeriocularViT_UBIPR.ckpt")
