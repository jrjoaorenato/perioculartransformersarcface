# Code for the paper "Combining ArcFace and Visual Transformer Mechanisms for Biometric Periocular Recognition"

In this repository, the code for the paper "Combining ArcFace and Visual Transformer Mechanisms for Biometric Periocular Recognition" is provided.

The datasets used are the following:

- FRGC
- UBIPr

The FRGC cropping can be done by running the notebook "crop_frgc.ipynb" after obtaining the list of files from the FRGC dataset metadata.

The instructions to run the algorithm can be found in the periocularNotebook.ipynb notebook.
