import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torch.utils.data import random_split
from torch.optim.lr_scheduler import ReduceLROnPlateau
import torchvision.models as models
from torchvision import transforms
from dataset import FRGCPeriocularDataModule
from dataset import UbiprDataModule
from models import PeriocularArcFace, PeriocularViT
from pytorch_metric_learning import losses
from pytorch_metric_learning import miners
from pytorch_metric_learning import regularizers
import os

import pytorch_lightning as pl

train_dataset = 'frgc'
test_dataset = 'ubipr'

batch_size = 128
# num_classes = 522 #ubipr
num_classes = 886 #frgc
embedding_size = 1024
ubipr_location = './data/UBIPR'

ubipr_dataset = UbiprDataModule(ubipr_location, batch_size=batch_size, complete=False, train = False)
#ubipr_dataset = UbiprDataModule(ubipr_location, batch_size=batch_size, complete = False, train=False)

model = PeriocularViT.load_from_checkpoint("/content/PeriocularRecognition/checkpoints/ViT2/efficient_former_l3.ckpt", num_classes=num_classes)
model.freeze()

trainer = pl.Trainer(gpus=1)
trainer.test(model, dataloaders=ubipr_dataset)

model.save_output('./transf_data/deit/', str(train_dataset + '2' + test_dataset))